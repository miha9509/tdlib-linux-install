# install TDLIB linux/ubuntu/mint
### Install programs:
1. sudo apt-get -y install cmake
2. sudo apt install -y ccache
3. sudo apt-get install libssl-dev
4. sudo apt install zlib1g-dev
5. sudo apt install gperf


### Build and install:
1. git clone https://github.com/tdlib/td.git —depth 1
2. cd td && mkdir build && cd build
3. cmake -DCMAKE_BUILD_TYPE=Release ..
4. cmake --build . -- -j5
5. sudo make install


### notes
- -j5, 5 - cores num +1
- if DCMAKE throw error, just start build again (its cache overflow)

